import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor() { }

  languages = [];
  castes = [];
  states = [];
  religions = [];
  countries = [];

  ngOnInit() {
    this.religions = ["Hindu","Jains","Muslims","Christian","Sikh","Parsi","Budshist","Jeish"];
    this.castes = ["Agarwal","Gupta","Arora","Baniya","Brahmin","Jat","Kayastha","Khatri","Rajput","Sunni"];
    this.states = ["Tamil Nadu","Kerala","Karnataka","Andhra Pradhesh","Telengana","Maharashtra","Bihar","Uttar Pradesh","West Bengal","Delhi","Madhya Pradesh","Orissa","Gujarat"];
    this.languages = ["Tamil","Malayalam","Kannada","Telugu","Hindi","Punjabi","Urdu","Bengali","Gujarati"];
    this.countries = ["India","USA","UK","Cannada","Australia"];
  }

}
