import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProfileDetailComponent } from './profile-detail/profile-detail.component';
import { AuthenticationService } from './home/authentication.service';
import { AuthGuardService } from './guards/auth-guard.service';
import { UserUpdateComponent } from './user-update/user-update.component';
import { OtpComponent } from './otp/otp.component';
import { ShareIdentityComponent } from './share-identity/share-identity.component';
import { UploadPhotoComponent } from './upload-photo/upload-photo.component';
import { MainComponent } from './main/main.component';
import { DashboardComponent } from './main/dashboard/dashboard.component';
import { PartnerPreferenceComponent } from './main/partner-preference/partner-preference.component';
import { RegistrationComponent } from './registration/registration.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent,
  children:[
    { path: 'detail', component: ProfileDetailComponent,canActivate:[AuthGuardService]}
  ] 
 },
  { path: 'userupdate', component: UserUpdateComponent },
  { path: 'register', component: RegistrationComponent },
  { path: 'otp', component: OtpComponent },
  { path: 'share-identity', component: ShareIdentityComponent },
  { path: 'upload-photo', component: UploadPhotoComponent },
  { path: 'main', component: MainComponent,
  children:[
    { path: 'dashboard', component: DashboardComponent},
    { path: 'preference', component: PartnerPreferenceComponent}
  ] 
},
  { path: '', redirectTo: 'home', pathMatch:'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
