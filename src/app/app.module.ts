import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { StoriesComponent } from './stories/stories.component';
import { SwiperModule } from 'angular2-useful-swiper';
import { RecentlyJoinedComponent } from './recently-joined/recently-joined.component';
import { FeaturesComponent } from './features/features.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { AppLinkComponent } from './app-link/app-link.component';
import { FooterComponent } from './footer/footer.component';
import { FormsModule,ReactiveFormsModule  }   from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { ProfileDetailComponent } from './profile-detail/profile-detail.component';
import { GlobalService } from './service/global.service';
import { AuthenticationService } from './home/authentication.service';
import { UserUpdateComponent } from './user-update/user-update.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { OtpComponent } from './otp/otp.component';
import { ShareIdentityComponent } from './share-identity/share-identity.component';
import { FileSelectDirective } from 'ng2-file-upload';
import { AngularFileUploaderModule } from "angular-file-uploader";
import { UploadPhotoComponent } from './upload-photo/upload-photo.component';
import { MainComponent } from './main/main.component';
import { DashboardComponent } from './main/dashboard/dashboard.component';
import { PartnerPreferenceComponent } from './main/partner-preference/partner-preference.component';
import { AdditionalFeaturesComponent } from './additional-features/additional-features.component';
import { ShortlistedComponent } from './main/dashboard/shortlisted/shortlisted.component';
import { ConnectionsComponent } from './main/dashboard/connections/connections.component';
import { InterestComponent } from './main/dashboard/interest/interest.component';
import { ProfileviewsComponent } from './main/dashboard/profileviews/profileviews.component';
import { RegistrationComponent } from './registration/registration.component';
import { BasicDetailsComponent } from './user-update/basic-details/basic-details.component';
import { EducationDetailsComponent } from './user-update/education-details/education-details.component';
import { FamilyDetailsComponent } from './user-update/family-details/family-details.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    StoriesComponent,
    RecentlyJoinedComponent,
    FeaturesComponent,
    PrivacyComponent,
    AppLinkComponent,
    FooterComponent,
    ProfileDetailComponent,
    UserUpdateComponent,
    OtpComponent,
    ShareIdentityComponent,
    FileSelectDirective,
    UploadPhotoComponent,
    MainComponent,
    DashboardComponent,
    PartnerPreferenceComponent,
    AdditionalFeaturesComponent,
    ShortlistedComponent,
    ConnectionsComponent,
    InterestComponent,
    ProfileviewsComponent,
    RegistrationComponent,
    BasicDetailsComponent,
    EducationDetailsComponent,
    FamilyDetailsComponent
  ],
  imports: [
    BsDatepickerModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    SwiperModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    AngularFileUploaderModule,
    BsDropdownModule.forRoot()
  ],
  providers: [GlobalService,AuthenticationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
